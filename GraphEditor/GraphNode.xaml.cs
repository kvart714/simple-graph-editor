﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GraphEditor
{
    /// <summary>
    /// Логика взаимодействия для GraphNode.xaml
    /// </summary>
    public partial class GraphNode : UserControl, IDragable
    {
        public GraphNode()
        {
            InitializeComponent();
            content.MouseDown += GraphNode_MouseDown;
            content.MouseUp += GraphNode_MouseUp;
            ellipse1.MouseDown += delegate
            {
                VisualStateManager.GoToState(this, "UnactivateState", true);
                MouseDown(this, null);
            };
            ellipse2.MouseDown += delegate
            {
                VisualStateManager.GoToState(this, "UnactivateState", true);
                var arrow = new Arrow
                {
                    StartPosition = position,
                    EndPosition = position + (Mouse.GetPosition(this) - new Point())
                };
                Arrows.Add(arrow);                
                var node = new GraphNode { Position = position + (Mouse.GetPosition(this) - new Point()) };                
                ArrowCreated(this, arrow);
                arrow.Drop(new Vector(), node);
                NodeCreated(this, node);
                
            };
            ellipse3.MouseDown += delegate
            {
                VisualStateManager.GoToState(this, "UnactivateState", true);
                var arrow = new Arrow
                {
                    StartPosition = position,
                    EndPosition = position + (Mouse.GetPosition(this) - new Point())
                };
                Arrows.Add(arrow);
                ArrowCreated(this, arrow);
            };
            ellipse4.MouseUp += delegate
            {
                VisualStateManager.GoToState(this, "UnactivateState", true);
                (Parent as Panel).Children.Remove(this);
                Arrows.ForEach(a => a.Drop(new Vector(), null));
                ArrowsEnds.ForEach(a => a.Drop(new Vector(), null));
            };
        }

        public List<Arrow> Arrows { get; private set; } = new List<Arrow>();
        public List<Arrow> ArrowsEnds { get; private set; } = new List<Arrow>();

        public event EventHandler<Arrow> ArrowCreated;
        public event EventHandler<GraphNode> NodeCreated;

        public new event EventHandler<MouseButtonEventArgs> MouseDown;

        private bool _selected;
        public bool IsSelected
        {
            get => _selected;
            set
            {
                _selected = value;
                VisualStateManager.GoToState(this, IsSelected ? "SelectedState" : "UnselectedState", true);
                VisualStateManager.GoToState(this, "ActivateState", true);
            }
        }

        bool click = false;
        private void GraphNode_MouseDown(object sender, MouseButtonEventArgs e)
        {
            click = true;
            MouseDown(this, e);
        }
        private void GraphNode_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (click)
            {
                IsSelected = !IsSelected;                
                click = false;
            }
        }        

        public void BeginDrag()
        {
            VisualStateManager.GoToState(this, "DragState", true);
            VisualStateManager.GoToState(this, "UnactivateState", true);
            click = false;
        }

        public void Drag(Vector delta)
        {
            SetCanvasPosition(position + delta);
            Arrows.ForEach(a => a.StartPosition = position + delta);
            ArrowsEnds.ForEach(a => a.EndPosition = position + delta);
        }

        public new void Drop(Vector delta, IDragable target)
        {
            Position += delta;
            VisualStateManager.GoToState(this, "DropState", true);
            ArrowsEnds.ForEach(a => a.EndPosition = position);
        }

        Point position;

        public Point Position
        {
            set
            {
                SetCanvasPosition(value);
                position = value;
            }
            get => position;
        }

        private void SetCanvasPosition(Point pos)
        {
            Canvas.SetLeft(this, pos.X);
            Canvas.SetTop(this, pos.Y);
        }
    }
}
