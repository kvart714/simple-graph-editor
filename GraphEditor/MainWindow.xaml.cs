﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GraphEditor
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            
        }

        IEnumerable<IDragable> Nodes => canvas.Children.Cast<IDragable>();
        List<IDragable> Dragables = new List<IDragable>();
        IDragable begin_dragable;
        Line dragable_line;
        //GraphNode dragable;
        private Point DragPoint;

        private void Canvas_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (!Nodes.Any(n => n.IsMouseOver))
            {
                var node = new GraphNode();
                node.Position = e.GetPosition(canvas);
                node.MouseDown += Node_MouseDown;
                node.ArrowCreated += Node_ArrowCreated;
                node.NodeCreated += Node_NodeCreated;
                //Nodes.Add(node);
                canvas.Children.Add(node);
            }
        }

        private void Node_NodeCreated(object sender, GraphNode node)
        {
            node.MouseDown += Node_MouseDown;
            node.ArrowCreated += Node_ArrowCreated;
            node.NodeCreated += Node_NodeCreated;
            begin_dragable = node;
            DragPoint = Mouse.GetPosition(canvas);
            canvas.Children.Add(node);
        }

        private void Node_ArrowCreated(object sender, Arrow e)
        {
            e.MouseDown += Node_MouseDown;
            begin_dragable = e;
            DragPoint = Mouse.GetPosition(canvas);
            canvas.Children.Add(e);
        }

        private void Node_MouseDown(object sender, MouseButtonEventArgs e)
        {
            begin_dragable = sender as IDragable;
            DragPoint = Mouse.GetPosition(canvas);
        }

        private void Canvas_MouseMove(object sender, MouseEventArgs e)
        {
            var delta = e.GetPosition(canvas) - DragPoint;

            if (begin_dragable != null)
            {                
                if (delta.Length > 5)
                {
                    Dragables = (begin_dragable as GraphNode)?.IsSelected == true ?
                        Nodes.Where(n => n.IsSelected).ToList() :
                        new List<IDragable> { begin_dragable };                   
                    
                    begin_dragable = null;

                    foreach (var dragable in Dragables)
                    {
                        dragable.BeginDrag();
                    }
                }
            }
            Dragables?.ForEach(d => d.Drag(e.GetPosition(canvas) - DragPoint));
            
        }

        private void canvas_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (Dragables != null)
            {
                Dragables?.ForEach(d => d.Drop(e.GetPosition(canvas) - DragPoint, Nodes.FirstOrDefault(n => n.IsMouseOver)));
                Dragables = null;
            }
            begin_dragable = null;
        }
    }
}
