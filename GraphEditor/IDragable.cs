﻿using System.Windows;

namespace GraphEditor
{
    public interface IDragable
    {
        void BeginDrag();
        void Drag(Vector delta);
        void Drop(Vector delta, IDragable target);
        bool IsSelected { get; set; }
        bool IsMouseOver { get; }
    }
}