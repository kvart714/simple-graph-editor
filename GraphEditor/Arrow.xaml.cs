﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GraphEditor
{
    /// <summary>
    /// Логика взаимодействия для Arrow.xaml
    /// </summary>
    public partial class Arrow : UserControl, IDragable
    {
        public Arrow()
        {
            InitializeComponent();
            Canvas.SetZIndex(this, -1);
        }        

        public bool IsSelected
        {
            get => false;
            set => throw new NotImplementedException();
        }

        //public new bool IsMouseOver => false;


        private Point startPoint;
        public Point StartPosition
        {
            set
            {
                startPoint = value;
                InitSize();
            }
        }

        private Point endPoint;
        public Point EndPosition
        {
            set
            {
                endPoint = value;
                InitSize();
            }
        }


        public void BeginDrag()
        {
            endPoint = Mouse.GetPosition(Parent as Panel);
            InitSize();
            //throw new NotImplementedException();
        }

        public void Drag(Vector delta)
        {
            InitSize(delta);
            //throw new NotImplementedException();
        }

        GraphNode end_node;
        public new void Drop(Vector delta, IDragable target)
        {
            target = target as GraphNode;
            if (target == null)
            {
                end_node?.ArrowsEnds.Remove(this);
                (Parent as Panel).Children.Remove(this);
            }
            else
            {
                var node = target as GraphNode;
                endPoint = new Point(Canvas.GetLeft(node), Canvas.GetTop(node));
                if (end_node != null)
                {
                    end_node.ArrowsEnds.Remove(this);
                }
                node.ArrowsEnds.Add(this);
                end_node = node;                
                InitSize();
            }
        }

        private void InitSize(Vector delta = default(Vector))
        {
            var ep = endPoint + delta;
            var v =  ep - startPoint;            
            Canvas.SetLeft(this, Math.Min(startPoint.X, ep.X));
            Canvas.SetTop(this, Math.Min(startPoint.Y, ep.Y));            
            Width = Math.Abs(v.X);
            Height = Math.Abs(v.Y);

            scale_transform.ScaleX = (v.X > 0) ^ (v.Y > 0) ? -1 : 1;
        }
    }
}
